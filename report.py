from numpy import int64
import pandas as pd
def op_input(dt):
  bmj=dt.loc[:, ~dt.columns.isin(['Charge*', 'Subtotal'])]#reading all columns except Charge and subtotal
  bmj.insert(4, 'Charge*',0)
  bmj.insert(5, 'Subtotal',0)
  return bmj

def mergng(bmj,data):
 bmj=pd.merge(bmj, data, on='Item type', how='inner')
 bmj['Charge*']=bmj['Cost (£)']
 del bmj['Cost (£)']
 bmj['Subtotal']=bmj['Charge*'].str.extract('(\d+)').astype(float)
 bmj['Subtotal']=pd.to_numeric(bmj['Subtotal'])
 bmj['Subtotal']=int64(bmj['Quantity or time taken (in h)'])*bmj['Subtotal']
 bmj.loc['Total'] = bmj[['Subtotal']].sum().reindex(bmj.columns, fill_value='')
 return bmj

# operation on input file
dt1=pd.read_csv("bmj.csv",engine='python')#reading input file 
data2 = dt1.rename(columns=dt1.iloc[0]).loc[1:]
data2['Item type'] =data2['Item type'].str.replace(r" \(.*\)","",regex=True)#removing the bracket words
data2['Item type']=data2['Item type'].str.upper()#changing the column "Itemtype" to uppercase
data2["Item type"] = data2['Item type'].str.replace('[^\w\s]','')
data2 = data2.dropna()
data2.drop_duplicates(keep=False,inplace=True)
bmj1=op_input(data2)

#Operation on available data
data=pd.read_csv("BMJ Ch21.csv",usecols=['Item type','Cost (£)'])
data["Item type"] = data['Item type'].str.replace('[^\w\s]','')
data['Item type']=data['Item type'].str.upper()

#merging dataframes
res=mergng(bmj1,data)
#print(res)
res.to_csv('Report.csv')


